// #include <ssstopwatch.hh>

// template <typename BaseTimer> SSStopwatch::basic_stopwatch<BaseTimer>::basic_stopwatch(bool start)
// {
//   if (start)
//     Start();
//   std::cerr << "TIMER CONSTRUCTOR"
//             << "\n";
// }

// template <typename BaseTimer> SSStopwatch::basic_stopwatch<BaseTimer>::~basic_stopwatch()
// {
//   std::cerr << "TIMER DESTRUCTOR"
//             << "\n";
// }

// template <typename BaseTimer>
// auto SSStopwatch::basic_stopwatch<BaseTimer>::LapGet() const -> unsigned
// {
//   return m_lap;
// }

// template <typename BaseTimer>
// auto SSStopwatch::basic_stopwatch<BaseTimer>::Show(std::string_view event_name) -> void
// {
//   if (m_is_running) {
//     m_log << "Stopwatch: " << event_name << ": (time: " << time << ", lap: " << m_lap << ")\n";
//   } else {
//     m_log << "Failed to show: \"" << event_name << "\", stopwatch is not currently running.\n";
//   }
// }

// template <typename BaseTimer>
// auto SSStopwatch::basic_stopwatch<BaseTimer>::Start(std::string_view event_name) -> unsigned
// {
//   if (not m_is_running) {
//     m_is_running = true;
//     m_log << event_name;
//     Start();
//     return ++m_lap;
//   }
//   m_log << "Failed to start: \"" << event_name << "\", stopwatch is currently running.\n";
//   return 0;
// }

// template <typename BaseTimer>
// auto SSStopwatch::basic_stopwatch<BaseTimer>::Stop(std::string_view event_name) -> unsigned
// {
//   if (m_is_running) {
//     m_is_running = false;
//     m_log << event_name << '\n';
//     Start();
//     return m_lap;
//   }
//   m_log << "Failed to stop: \"" << event_name << "\", stopwatch is not running.\n";
//   return 0;
// }
