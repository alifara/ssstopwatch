#pragma once

#include <chrono>
#include <ostream>
#include <string_view>

namespace SSStopwatch {
using namespace std::string_literals;

// clang-format off
template <typename T>
concept BaseTimer =
  std::is_default_constructible<T>::value and
  std::is_trivially_destructible<T>::value and
  requires(T instance)
{
  { instance.Clear() }     -> std::same_as<void>;
  { instance.Start() }     -> std::same_as<void>;
  { instance.IsStarted() } -> std::same_as<bool>;
  { instance.GetMS() }     -> std::same_as<unsigned long>;
};
// clang-format on

template <BaseTimer T> class basic_stopwatch : T {

public:
  // create, optionally start timing an activity
  explicit basic_stopwatch(bool start);
  explicit basic_stopwatch(std::string_view activity = "Stopwatch"s, bool start = true);
  basic_stopwatch(std::ostream& log, std::string_view activity = "Stopwatch"s, bool start = true);

  // stop and destroy a stopwatch
  ~basic_stopwatch();

  // get last lap time (time of last stop)
  unsigned LapGet() const;

  // predicate: return true if the stopwatch is running
  bool IsStarted() const;

  // show accumulated time, keep running, set/return lap
  void Show(std::string_view event_name = "show"s);
  // unsigned Show(std::string_view event_name= "show"s);

  // (re)start a stopwatch, set/return lap time
  unsigned Start(std::string_view event_name = "start"s);

  // stop a running stopwatch, set/return lap time
  unsigned Stop(std::string_view event_name = "stop"s);

  // NOTE: do we need a restart method?
private:
  // members
  bool m_is_running;
  std::string_view m_activity; // "activity" string
  unsigned m_lap; // lap time (time of last stop)
  std::ostream& m_log; // stream on which to log events
};

class TimerBaseClock {
public:
  // clears the timer
  TimerBaseClock() { m_start = -1; }

  // clears the timer
  void Clear() { m_start = -1; }

  // returns true if the timer is running
  bool IsStarted() const { return (m_start != -1); }

  // start the timer
  void Start() { m_start = clock(); }

  // get the number of milliseconds since the timer was started
  unsigned long GetMS()
  {
    clock_t now;
    if (IsStarted()) {
      now = clock();
      clock_t dt = (now - m_start);
      return (unsigned long)(dt * 1000 / CLOCKS_PER_SEC);
    }
    return 0;
  }

private:
  clock_t m_start;
};

class TimerBaseChrono { //
public:
  // clears the timer
  TimerBaseChrono()
    : m_start(std::chrono::system_clock::time_point::min())
  {
  }

  // clears the timer
  void Clear() { m_start = std::chrono::system_clock::time_point::min(); }

  // returns true if the timer is running
  bool IsStarted() const { return (m_start != std::chrono::system_clock::time_point::min()); }
  // bool IsStarted() const
  // {
  //   return (m_start.time_since_epoch() != std::chrono::system_clock::duration(0));
  // }
  //
  // start the timer
  void Start() { m_start = std::chrono::system_clock::now(); }
  //
  // get the number of milliseconds since the timer was started
  unsigned long GetMS()
  {
    if (IsStarted()) {
      auto diff = std::chrono::system_clock::now() - m_start;
      return (unsigned)(std::chrono::duration_cast<std::chrono::milliseconds>(diff).count());
    }
    return 0;
  }

private:
  std::chrono::system_clock::time_point m_start;
};

using Stopwatch = basic_stopwatch<TimerBaseClock>; // TODO: write something appropriate
}

template <typename BaseTimer>
auto SSStopwatch::basic_stopwatch<BaseTimer>::LapGet() const -> unsigned
{
  return m_lap;
}

template <typename BaseTimer>
auto SSStopwatch::basic_stopwatch<BaseTimer>::Show(std::string_view event_name) -> void
{
  if (m_is_running) {
    m_log << "Stopwatch: " << event_name << ": (time: " << time << ", lap: " << m_lap << ")\n";
  } else {
    m_log << "Failed to show: \"" << event_name << "\", stopwatch is not currently running.\n";
  }
}

template <typename BaseTimer>
auto SSStopwatch::basic_stopwatch<BaseTimer>::Start(std::string_view event_name) -> unsigned
{
  if (not m_is_running) {
    m_is_running = true;
    m_log << event_name;
    Start();
    return ++m_lap;
  }
  m_log << "Failed to start: \"" << event_name << "\", stopwatch is currently running.\n";
  return 0;
}

template <typename BaseTimer>
auto SSStopwatch::basic_stopwatch<BaseTimer>::Stop(std::string_view event_name) -> unsigned
{
  if (m_is_running) {
    m_is_running = false;
    m_log << event_name << '\n';
    Start();
    return m_lap;
  }
  m_log << "Failed to stop: \"" << event_name << "\", stopwatch is not running.\n";
  return 0; 
}
