#include <catch2/catch.hpp>

#include <ssstopwatch.hh>

#include <iostream>
#include <thread>

TEST_CASE("Quick check", "[main]")
{
  {
    SSStopwatch::Stopwatch ws;

    using namespace std::chrono_literals;
    std::cout << "start\n";
    std::this_thread::sleep_for(600ms);
    std::cout << "end\n";
  }

  float mean = 2.0;
  REQUIRE(mean == 2.0);
  // CHECK(false);
}
